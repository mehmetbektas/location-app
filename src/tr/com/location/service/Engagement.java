package tr.com.location.service;

import tr.com.location.dto.Location;

public class Engagement {
	
	private Location locationTrack = new Location(39.90, 37.70, 500);
	private Location locationMLS = new Location(35.50, 34.40, 500);
	
	/*Burada, muhtemel buluşma noktasını, izin yakınında bir yer veriyoruz. İzin yönünü hesaba katmadık.*/
	private Location locationPIP = new Location(locationTrack.getX()+0.5, locationTrack.getY() + 0.5, 500);

	/*Atış sonrasında, ekrandaki zaman çizgisi 150 saniyeyi göstermektedir.*/
	private static final int STEP_COUNT = 150;
	
	public void run() {
		System.out.println("locationTrack: " + locationTrack);
		System.out.println("locationMLS: " + locationMLS);
		System.out.println("locationPIP: " + locationPIP);
		
		double distanceX = Math.abs(locationMLS.getX() - locationPIP.getX());
		double distanceY = Math.abs(locationMLS.getY() - locationPIP.getY());
		
		double distanceUnitX = distanceX / STEP_COUNT;
		double distanceUnitY = distanceY / STEP_COUNT;
		
		Location locationMissile = new Location(locationMLS.getX(),locationMLS.getY(), locationMLS.getZ());
		
		System.out.println("locationMissile: " + locationMissile);
		
		for (int i = 1; i <= STEP_COUNT; i++) {
			locationMissile.setX(locationMissile.getX() + distanceUnitX);
			locationMissile.setY(locationMissile.getY() + distanceUnitY);
			System.out.println("locationMissile: " + locationMissile);
		}
		
		
		
		
	}
	


}
