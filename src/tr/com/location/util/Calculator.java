package tr.com.location.util;

import tr.com.location.dto.Location;

public class Calculator {

	/*This method is never used.*/
	public static double calculateDistance(Location location1, Location location2) {
		return Math.sqrt(
				Math.pow(location1.getX() - location2.getX(), 2) + 
				Math.pow(location1.getY() - location2.getY(), 2)
			);
	}
}
